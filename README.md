This project integrates Reportportal.io with TestNG based on this link with screenshots:

https://github.com/reportportal/example-java-TestNG/blob/master/README.md#reportportal-integration-with-testng

To view the test results for the Selenium tests in the folder ./tests/browser/BrowserTest.java

1. Run the Runner.java file to execute the tests.

2. Login with the user added to the project MY_TEST_EXAMPLE e.g. mark in the localhost:8080.

3. Go to Launches folder to view the results for mark_TEST_EXAMPLE
