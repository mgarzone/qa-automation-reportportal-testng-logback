package com.epam.rp.tests.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by Mark Garzone
 */
public class BrowserTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BrowserTest.class);

    public WebDriver driver;
    public String testURL = "https://sonalake.com/";

    public By MENU_ITEM_HOME_SELECTOR = By.id("menu-item-1032");
    public By MENU_ITEM_ABOUT_SELECTOR = By.id("menu-item-1094");
    public By MENU_ITEM_SERVICES_SELECTOR = By.id("menu-item-1151");
    public By MENU_ITEM_OUR_WORK_SELECTOR = By.id("menu-item-1109");
    public By MENU_ITEM_CAREERS_SELECTOR = By.id("menu-item-1351");
    public By MENU_ITEM_NEWS_BLOG_SELECTOR = By.id("menu-item-1368");

    public String MENU_ITEM_HOME = "HOME";
    public String MENU_ITEM_ABOUT = "ERROR ABOUT";
    public String MENU_ITEM_SERVICES = "SERVICES";
    public String MENU_ITEM_OUR_WORK = "OUR WORK";
    public String MENU_ITEM_CAREERS = "CAREERS";
    public String MENU_ITEM_NEWS_BLOG = "NEWS & BLOG";


    public String PAGE_TITLE_HOME = "Sonalake - Your Software Innovation Partner";
    public String PAGE_TITLE_ABOUT = "ERROR About Sonalake - software product development, software engineering";
    public String PAGE_TITLE_SERVICES = "Services - Sonalake";
    public String PAGE_TITLE_OUR_WORK = "Our work - examples of our customer engagements and projects";
    public String PAGE_TITLE_CAREERS = "Software Development Careers at Sonalake";
    public String PAGE_TITLE_NEWS_BLOG =  "Latest @ Sonalake - Visual Analytics, Software development and IT monitoring";

    public String PAGE_TITLE_ERROR = "Title assertion is failed!";
    public String MENU_ITEM_ERROR = "Menu item assertion is failed!";


    @BeforeTest
    public void setupTest (ITestContext context){

        final String dir = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", dir + "/src/main/resources/chromedriver");

        driver = new ChromeDriver();

        // This is needed for the ScreenshotListener.
        context.setAttribute("driver", driver);

        driver.navigate().to(testURL);
    }


    @Test
    public void verifyMenuItemHomeTest () {

        String actualText = driver.findElement(MENU_ITEM_HOME_SELECTOR).getText();
        LOGGER.debug("Menu Item: " + actualText);
        Assert.assertEquals(actualText, MENU_ITEM_HOME, MENU_ITEM_ERROR);
    }

    @Test
    public void verifyMenuItemAboutTest () {

        String actualText = driver.findElement(MENU_ITEM_ABOUT_SELECTOR).getText();
        LOGGER.debug("Menu Item: " + actualText);
        Assert.assertEquals(actualText, MENU_ITEM_ABOUT, MENU_ITEM_ERROR);
    }

    @Test
    public void verifyMenuItemServicesTest () {

        String actualText = driver.findElement(MENU_ITEM_SERVICES_SELECTOR).getText();
        LOGGER.debug("Menu Item: " + actualText);
        Assert.assertEquals(actualText, MENU_ITEM_SERVICES, MENU_ITEM_ERROR);
    }

    @Test
    public void verifyMenuItemOurWorkTest () {

        String actualText = driver.findElement(MENU_ITEM_OUR_WORK_SELECTOR).getText();
        LOGGER.debug("Menu Item: " + actualText);
        Assert.assertEquals(actualText, MENU_ITEM_OUR_WORK, MENU_ITEM_ERROR);
    }

    @Test
    public void verifyMenuItemCareersTest () {

        String actualText = driver.findElement(MENU_ITEM_CAREERS_SELECTOR).getText();
        LOGGER.debug("Menu Item: " + actualText);
        Assert.assertEquals(actualText, MENU_ITEM_CAREERS, MENU_ITEM_ERROR);
    }

    @Test
    public void verifyMenuItemNewsAndBlogTest () {

        String actualText = driver.findElement(MENU_ITEM_NEWS_BLOG_SELECTOR).getText();
        LOGGER.debug("Menu Item: " + actualText);
        Assert.assertEquals(actualText, MENU_ITEM_NEWS_BLOG, MENU_ITEM_ERROR);
    }


    @Test
    public void clickMenuItemAboutTest () {

        driver.findElement(MENU_ITEM_ABOUT_SELECTOR).click();
        String actualTitle = driver.getTitle();
        LOGGER.debug("Page Title: " + actualTitle);
        Assert.assertEquals(actualTitle, PAGE_TITLE_ABOUT, PAGE_TITLE_ERROR);

    }

    @Test
    public void clickkMenuItemServicesTest () {

        driver.findElement(MENU_ITEM_SERVICES_SELECTOR).click();
        String actualTitle = driver.getTitle();
        LOGGER.debug("Page Title: " + actualTitle);
        Assert.assertEquals(actualTitle, PAGE_TITLE_SERVICES, PAGE_TITLE_ERROR);
    }

    @Test
    public void clickMenuItemOurWorkTest () {

        driver.findElement(MENU_ITEM_OUR_WORK_SELECTOR).click();
        String actualTitle = driver.getTitle();
        LOGGER.debug("Page Title: " + actualTitle);
        Assert.assertEquals(actualTitle, PAGE_TITLE_OUR_WORK, PAGE_TITLE_ERROR);
    }

    @Test
    public void clickMenuItemHomeTest () {

        driver.findElement(MENU_ITEM_HOME_SELECTOR).click();
        String actualTitle = driver.getTitle();
        LOGGER.debug("Page Title: " + actualTitle);
        Assert.assertEquals(actualTitle, PAGE_TITLE_HOME, PAGE_TITLE_ERROR);
    }

    @Test
    public void clickMenuItemCareersTest () {

        driver.findElement(MENU_ITEM_CAREERS_SELECTOR).click();
        String actualTitle = driver.getTitle();
        LOGGER.debug("Page Title: " + actualTitle);
        Assert.assertEquals(actualTitle, PAGE_TITLE_CAREERS, PAGE_TITLE_ERROR);
    }

    @Test
    public void clickMenuItemNewsAndBlogTest () {

        driver.findElement(MENU_ITEM_NEWS_BLOG_SELECTOR).click();
        String actualTitle = driver.getTitle();
        LOGGER.debug("Page Title: " + actualTitle);
        Assert.assertEquals(actualTitle, PAGE_TITLE_NEWS_BLOG, PAGE_TITLE_ERROR);
    }

    @AfterTest
    public void teardownTest (){
        //Close browser and end the session
        driver.quit();
    }


}
