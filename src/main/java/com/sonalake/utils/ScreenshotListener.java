package com.sonalake.utils;


import com.epam.rp.tests.LoggingUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.util.Objects.nonNull;


public class ScreenshotListener extends TestListenerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScreenshotListener.class);

    @Override
    public void onTestFailure(ITestResult result) {
        final Throwable throwable = result.getThrowable();

        if (throwable != null) {
            LOGGER.error("--- TEST FAILED with exception: ", throwable);
        } else {
            LOGGER.error("--- TEST FAILED ---");
        }
        try {
            LOGGER.info("Taking screenshot");
            ITestContext context = result.getTestContext();
            WebDriver driver = (WebDriver)context.getAttribute("driver");

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
            String methodName = result.getName();

            if (nonNull(driver)) {
                final File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

                String reportDirectory = new File(System.getProperty("user.dir")).getAbsolutePath() + "/target/surefire-reports";
                File destFile = new File((String) reportDirectory+"/failure_screenshots/"+methodName+"_"+formater.format(calendar.getTime())+".png");
                 FileUtils.copyFile(scrFile, destFile);


               LoggingUtils.log(destFile, "Sending screenshot to reportportal.io");
            } else {
                LOGGER.error("Unable to take screenshot - web driver is null");
            }

        } catch (final Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

}
